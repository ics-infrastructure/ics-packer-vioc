ics-paker-vioc
==============

This [Packer](https://www.packer.io) template creates a [vagrant](https://www.vagrantup.com) box to run Virtual IOCs.

License
-------

BSD 2-clause
